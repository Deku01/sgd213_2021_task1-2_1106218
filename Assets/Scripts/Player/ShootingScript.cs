﻿using UnityEngine;
using System.Collections;

public class ShootingScript : MonoBehaviour
{

    private ObjectPool objectPool;

    private float lastFiredTime = 0f;

    [SerializeField]
    private float fireDelay = 1f;

    private float bulletOffset = 2f;

    void Start()
    {
        objectPool = GetComponent<ObjectPool>();

        // Do some math to perfectly spawn bullets in front of us
        bulletOffset = GetComponent<Renderer>().bounds.size.y / 2 // Half of our size
            + objectPool.Pooledtype.GetComponent<Renderer>().bounds.size.y / 2; // Plus half of the bullet size
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            float currentTime = Time.time;

            // Have a delay so we don't shoot too many bullets
            if (currentTime - lastFiredTime > fireDelay)
            {
                Vector2 spawnLocation = new Vector2(transform.position.x, transform.position.y + bulletOffset);

                objectPool.GetObject(spawnLocation, transform.rotation);
                //Instantiate(objectPool.pooledType, spawnLocation, transform.rotation);

                lastFiredTime = currentTime;
            }
        }
    }
}
