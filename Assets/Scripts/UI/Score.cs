﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : MonoBehaviour
{

    [SerializeField]
    private Text scoreText;

    private int score = 0;

    // Use this for initialization
    public void Start()
    {
        scoreText = GetComponent<Text>();
    }

    // Update is called once per frame
    public void Update()
    {
        scoreText.text = string.Format("Score:  {0}", score);
    }

    public void AddScore(int addition)
    {
        score += addition;
    }
}
